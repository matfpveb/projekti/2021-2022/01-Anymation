const mongoose = require('mongoose');

// we should change it so that our mongo uri is in the env
const connectionString = process.env.MONGO_URI;

mongoose
    .connect(connectionString, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => console.log('MongoDB Connected'))
    .catch((e) => {
        console.error('MongoDB connection error: ', e.message);
    });

const db = mongoose.connection;

module.exports = db;
