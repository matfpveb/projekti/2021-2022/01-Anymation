const express = require("express");
const bodyParser = require("body-parser");
const cors = require('cors');
require("dotenv").config();

const db = require("./db");

// all the routes we will use
const showRouter = require("./routes/showRouter");
const userRouter = require("./routes/userRouter");
const userRatingsRouter = require("./routes/userRatingsRouter");
const app = express();

app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());

db.on("error", console.error.bind(console, 'MongoDB connection error:'));

app.use(cors());

app.get("/", (req, res) => {
    console.log("Hello!");
});

// connecting the routers and the urls

app.use("/api/shows", showRouter);
app.use("/api/user", userRouter);
app.use("/api/userRatings", userRatingsRouter);

module.exports = app;

