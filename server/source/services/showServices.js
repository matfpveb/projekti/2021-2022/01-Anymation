const mongoose = require("mongoose");
const Show = require("../models/showModel");

const getAllShows = async () => {
    const shows = await Show.find({}).exec();
    return shows;
};

const getShowById = async (id) => {
    const show = await Show.findById(id).exec();
    return show;
};

const getShowByTitle = async (title) => {
    const show = await Show.find({title: title}).exec();
    return show;
};

const getShowByIMDBScore = async () => {
    const show = await Show.find({}).sort({ score: -1 }).exec();
    return show;
};
const getShowByScore = async () => {
    const show = await Show.find({}).sort({ our_score: -1 }).sort({title : 1}).exec();
    return show;
};

const getAllShowsStartWith = async (startingName) => {
    const shows = await Show.find({title : {$regex : new RegExp("^" + startingName, "i")}});
    return shows;
};

const getShowsByGenre = async (nameOfGenre) => {
    const shows = await Show.find({genre : nameOfGenre}).sort({title : 1}).exec();
    return shows;
};

const getAllShowsByGenre = async (nameOfGenre,page=1,limit=9) => {
    const shows = await Show.paginate({ genre: nameOfGenre },{page,limit});
    return shows;
};

const getShowByScorePaginate = async (name, page=1,limit=9) => {
    const shows = await Show.paginate({},{page,limit,sort:{our_score: -1, title : 1}});
    return shows;
};

const getShowByIMDBScorePaginate = async (name, page=1,limit=9) => {
    const shows = await Show.paginate({},{page,limit,sort:{ score: -1, title : 1}});
    return shows;
};

const getShowByHotPaginate = async (name, page=1,limit=9) => {
    const shows = await Show.paginate({},{page,limit,sort:{ favorite_number: -1, title : 1}});
    return shows;
};

const increaseFavouriteNumber = async (name, number) => {
    const filter = {title : name};
    const update = { $inc: {favorite_number : 1}};
    const show = await Show.findOneAndUpdate(filter, update, {
      new : true
    }).exec();
    return show;
}

const increaseViewNumber = async (name, number) => {
    const filter = {title : name};
    const update = { $inc: {our_view_number : 1}};
    const show = await Show.findOneAndUpdate(filter, update, {
      new : true
    }).exec();
    return show;
}

  
module.exports = {
    getAllShows,
    getShowById,
    getShowByTitle,
    getAllShowsStartWith,
    getShowByIMDBScore,
    getShowByScore,
    getAllShowsByGenre,
    getShowByScorePaginate,
    getShowByIMDBScorePaginate,
    getShowByHotPaginate,
    increaseFavouriteNumber,
    getShowsByGenre,
    increaseViewNumber
};
  
