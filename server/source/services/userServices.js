const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const User = require('../models/userModel');
const Show = require('../models/showModel');

const getAllUsers = async () => {
  const users = await User.find({}).exec();
  return users;
};

const getUserById = async (id) => {
  const user = await User.findById(id).exec();
  return user;
};

const getUserByUsername = async (username) => {
  const user = await User.findOne({ username : username }).exec();
  return user;
};

const getCounterShows = async (username) => {
  const user = await User.findOne({ username : username }).exec();
  return user.counterShows;
};


const updateCounterShowByUsername = async (username) => {
  const filter = {username : username};
  const update = { $inc: {counterShows : 1}};
  const user = await User.findOneAndUpdate(filter, update, {
      new : true
    }).exec();
  return user;
};

const getUsersByUsernamePrefix = async (username) => {
  const users = await User.find({username : {$regex : new RegExp("^" + username, "i")}}).exec();
  return users;
};

const registerUser = async (username, email, password) => {
  const newUser = new User({
    _id: new mongoose.Types.ObjectId(),
    username,
    email,
    password,
  });

  await newUser.save();
  return newUser;
};

const deleteUser = async (username) => {
  await User.findOneAndDelete({ username: username }).exec();
};

const updateUserData = async (oldUsername, newUsername, password, email) => {

  const filter = {username : oldUsername};
  const update = {username : newUsername, password : password, email : email};
  const user = await User.findOneAndUpdate(filter, update, {
    new : true
  }).exec();

  return user;

}

const updateUserShows = async (username, titleOfShow) => {

  const filter = {username : username};
  const update = { $addToSet: {favouriteShows: titleOfShow }};
  const user = await User.findOneAndUpdate(filter, update, {
    new : true
  }).exec();

  return user;

}

const getFavouriteShows = async (username) => {
  const user = await User.findOne({ username: username }).exec();
  const shows = user.favouriteShows;
  const favouriteShows = await Show.find({ title: { "$in" : shows } }).sort({ title : 1 }).exec();

  return favouriteShows;
};

const getFavouriteGenre = async (username) => {
  const user = await User.findOne({ username: username }).exec();
  const shows = user.favouriteShows;
  const genreList = [ "Adventure", "Comedy", "Family", "Fantasy", "Musical", "Romance" , "Shounen", "Musical", "Drama",
                      "Supernatural", "School", "Mystery", "Police", "Psychological", "Thriller", "Super Power", "Martial Arts",
                      "Action", "Shoujo", "Sci-Fi", "Magic", "Military", "Historical", "Sports", "Slice of Life", "Seinen", "Horror", "Car" ];
  let genreFav = "";
  let maxValue = -1;
  for (genre of genreList){
    let current = await Show.find({ title: { "$in" : shows }, genre : genre }).count().exec();
    if (current > maxValue){
      genreFav = genre;
      maxValue = current;
    } 
  }
  return genreFav;
};



module.exports = {
  getAllUsers,
  getUserById,
  getUserByUsername,
  getUsersByUsernamePrefix,
  registerUser,
  deleteUser,
  updateUserData,
  getFavouriteShows,
  updateUserShows,
  getFavouriteGenre,
  updateCounterShowByUsername,
  getCounterShows
};
