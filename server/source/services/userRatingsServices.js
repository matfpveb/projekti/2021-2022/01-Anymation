const mongoose = require("mongoose");
const UserRating = require("../models/userRatingsModel");
const Show = require("../models/showModel");

const getShowRatings = async (name) => {
    const ratings = await UserRating.find({ showname: name });
    return ratings;
}

const updateShowAverageRating = async (showname) => {
    const averageRatings = await UserRating.aggregate([{ $group: { _id: "$showname", avgScore: { $avg: "$rating" } } },{$match : {_id:showname}}]); //.aggregate([{ $group: { _id: "$showtitle", avgScore: { $avg: "$rating" } } }]);
    let newScore;
    if(averageRatings[0] != undefined)
        newScore = averageRatings[0].avgScore;
    else
        newScore = 0;
    const filter = {title : showname};
    const update = {our_score : newScore};
    const averageRating = await Show.findOneAndUpdate(filter,update,{
        new: true
    }).exec();
    return averageRating;
}

const getShowsRatedByUser = async (name) => {
    const userRatings = await UserRating.find({ username: name });
    return userRatings;
}

const addNewRating = async (showname, username, rating) => {
    const newRating = new UserRating({
        _id: new mongoose.Types.ObjectId(),
        username,
        showname,
        rating,
      });
    
      await newRating.save();
      return newRating;
}

const updateUserRating = async (title, username, newScore) => {
    const filter = {username : username, showname : title};
    const update = {rating : newScore};
    const userRating = await UserRating.findOneAndUpdate(filter, update, {
      new : true
    }).exec();
  
    return userRating;
  
  }

const getTopRatedShow = async (username) => {
    const userRating = await UserRating.find({username : username}).sort({ rating: -1 }).sort({showname :  1}).exec();
    let title;
    if(userRating[0] != undefined)
        title = userRating[0].showname;
    else
        title = "None";
    return title;
}

const getSpecificRating = async (username,showname) => {
    const userRating = await UserRating.findOne({ username: username, showname: showname });
    return userRating;
}

const removeUserRatings = async (username) => {
    const filter = {username: username};
    const userRatings = await UserRating.deleteMany(filter);
    return userRatings;
}

const updateUserRatingsUsername = async (oldUsername,newUsername) => {
    const filter = {username: oldUsername}
    const update = {username: newUsername}
    const userRatings = await UserRating.updateMany(filter,update, {
        new : true
      }).exec();
    return userRatings;
}

module.exports = {
    getShowRatings,
    updateShowAverageRating,
    getShowsRatedByUser,
    addNewRating,
    updateUserRating,
    getTopRatedShow,
    getSpecificRating,
    removeUserRatings,
    updateUserRatingsUsername
};