const jwt = require("jsonwebtoken");
const jwtOpts = { algorithm: 'HS256', expiresIn: '30d' };


const checkToken = (req, res, next) => {

    const token = req.header("x-auth-token");
    //console.log(token);

    if (!token) {
        return res.status(401).json("No token provided!");
    }

    jwt.verify(token, process.env.MY_SECRET, (err, payloadData) => {
        if (err) 
            return res
            .status(500)
            .json({ auth: false, message: "While checking the token, an error occured!" });

        //console.log(payloadData);

        req.username = payloadData.username;

        //const username = client.username;
        //const admin = client.admin;
        //req.payload = { username, admin };

        // why would we siign in again? and get another token??
        //req.newToken = jwt.sign(req.payload, process.env.ACCESS_TOKEN_SECRET, jwtOpts);

        next();

    });

};

module.exports = {
    checkToken,
};