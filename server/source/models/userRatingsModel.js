const mongoose = require("mongoose");

const userRatingSchema = new mongoose.Schema({
    _id : mongoose.Schema.Types.ObjectId,
    username : {
        type : String,
        required : true
    },
    showname : {
        type : String,
        required : true
    },
    rating : {
        type : Number,
        default : 0
    }
  
});

const UserRating = mongoose.model("userRating", userRatingSchema);
module.exports = UserRating;