const mongoose = require("mongoose");
const mongoosePaginate = require('mongoose-paginate-v2');


// might change the atributes in the future
// we dont want all of those atributes inside our db, so we will have to change it
const showSchema = new mongoose.Schema({
    _id : mongoose.Schema.Types.ObjectId,
    title : {
        type : String,
        required : true
    },
    score : {
        type : String,
        required : true
    },
    genre : {
        type : [String],
        required : true
    },
    year : {
        type : String,
    },
    episodes : {
        type : String
    },
    img_url : {
        type : String,
        required : true
    },
    description : {
        type : String,
    }, 
    our_view_number : {
        type : Number,
        default : 0
    },
    our_score : {
        type : Number,
        default : 0
    },
    favorite_number : {
        type : Number, 
        default : 0
    }
});

showSchema.plugin(mongoosePaginate);
const Show = mongoose.model("shows", showSchema);
module.exports = Show;


