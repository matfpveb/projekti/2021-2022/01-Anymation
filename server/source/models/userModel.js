const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    _id : mongoose.Schema.Types.ObjectId,
    username : {
        type : String,
        required : true
    },
    password : {
        type : String,
        required : true
    }, 
    email : {
        type : String,
        required : true
    },
    counterShows : {
        type : Number,
        default : 0
    },
    favouriteShows : {
        type :  [String],
        default : []
    }
  
});

const User = mongoose.model("user", userSchema);
module.exports = User;




