const express = require("express");

const userController = require("../controllers/userController.js");

const {checkToken} = require("../utils/token.js");

const router = express.Router();


// to get call "/api/user"
router.get("/", userController.getAllUsers);
router.get('/id/:id', userController.getUserById);
router.get('/username/:username', userController.getUserByUsername);
router.get('/usernamePrefix/:usernamePrefix', userController.getUsersByUsernamePrefix);
router.get('/favouriteShows/:username', userController.getFavouriteShows);
router.get('/favouriteGenre/:username', userController.getFavouriteGenre);
router.get('/getCounterShows/:username', userController.getCounterShows);

router.post('/login', userController.loginUser);
router.post('/register', userController.registerUser);
router.patch('/info', checkToken, userController.changeUserInfo);
router.patch('/favouriteShowsUpdate', userController.updateUserShows);
router.patch('/counterShowsUpdate', userController.updateCounterShows);
router.delete('/deleteUser', checkToken, userController.deleteUser);


module.exports = router;