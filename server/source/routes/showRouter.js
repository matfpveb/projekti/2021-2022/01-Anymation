const express = require("express");

const showController = require("../controllers/showController.js");

const router = express.Router();


// "/api/shows" is the starting url
router.get("/", showController.getAllShows);
router.get("/title/:title", showController.getShowByTitle);
router.get("/titlePrefix/:titlePrefix", showController.getAllShowsStartWith);
router.get("/IMDBScore", showController.getShowByIMDBScore);
router.get("/Score", showController.getShowByScore);
router.get("/Genre/:nameOfGenre/",showController.getAllShowsByGenre);
router.get("/FavouriteGenre/:nameOfGenre/",showController.getShowsByGenre);
router.get("/:title", showController.getShowByTitle);
router.get("/Score/:all/", showController.getShowByScorePaginate);
router.get("/IMDBScore/:all/", showController.getShowByIMDBScorePaginate);
router.get("/Hot/:all/", showController.getShowByHotPaginate);

router.patch("/increaseFavourite/", showController.increaseFavouriteNumber);
router.patch("/increaseView/", showController.increaseViewNumber);



module.exports = router;