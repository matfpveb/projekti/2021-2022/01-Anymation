const express = require("express");

const userRatingsController = require("../controllers/userRatingsController.js");

const router = express.Router();

router.get("/getShowRatings", userRatingsController.getShowRatings);
router.patch("/updateShowAverageRating", userRatingsController.updateShowAverageRating);
router.get("/getShowsRatedByUser/:username", userRatingsController.getShowsRatedByUser);
router.get("/getTopRatedShow/:username", userRatingsController.getTopRatedShow);
router.get("/getSpecificRating/:username/:showname", userRatingsController.getSpecificRating)
router.post("/addNewRating", userRatingsController.addNewRating);
router.patch("/updateUserRating", userRatingsController.updateUserRating);
router.patch("/removeUserRatings", userRatingsController.removeUserRatings);
router.patch("/updateUserRatingsUsername", userRatingsController.updateUserRatingsUsername);

module.exports = router;


