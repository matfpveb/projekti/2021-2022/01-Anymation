const mongoose = require("mongoose");
const db = require("../db/index.js");
const User = require("../models/userModel.js");
const validator = require('validator');
const usersService = require('../services/userServices');

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const jwtOpts = { algorithm: 'HS256', expiresIn: '30d' };
const saltRounds = 10;

const getAllUsers = async (req, res, next) => {
  try {
    const allUsers = await usersService.getAllUsers();
    res.status(200).json(allUsers);
  }
  catch (error) {
    next(error);
  }
};

const getUserById = async (req, res, next) => {
  const id = req.params.id;

  try {
    if (id == undefined) {
      const error = new Error('Missing id!');
      error.id = 400;
      throw error;
    }

    const user = await usersService.getUserById(id);
    if (user == null) {
      res.status(404).json();
    } else {
      res.status(200).json(user);
    }
  }
  catch (error) {
    next(error);
  }
};

const getUserByUsername = async (req, res, next) => {
  const username = req.params.username;
  try {
    if (username == undefined) {
      const error = new Error('Missing username!');
      error.status = 400;
      throw error;
    }
    const user = await usersService.getUserByUsername(username);
    if (user == null) {
      res.status(404).json();
    } else {
      res.status(200).json(user);
    }
  }
  catch (error) {
    next(error);
  }
};

const getUsersByUsernamePrefix = async (req, res, next) => {
  const username = req.params.usernamePrefix;
  try {
    if (username == undefined) {
      const error = new Error('Missing username!');
      error.status = 400;
      throw error;
    }
    const users = await usersService.getUsersByUsernamePrefix(username);
    if (users == null) {
      res.status(404).json();
    } else {
      res.status(200).json(users);
    }
  }
  catch (error) {
    next(error);
  }
}

const deleteUser = async (req, res, next) => {
  const username = req.body.username;

  try {
    if (!username) {
      const error = new Error('Missing username!');
      error.status = 400;
      throw error;
    }

    const user = await usersService.getUserByUsername(username);
    if (!user) {
      const error = new Error('Wrong username!');
      error.status = 404;
      throw error;
    }

    await usersService.deleteUser(username);
    res.status(200).json({ success: true });
  } catch (error) {
    next(error);
  }
};

const loginUser = async (req, res, next) => {
  const username = req.body.username;
  const password = req.body.password;

  try {
    if (!username || !password) {
      res.status(400).json("Params never sent!");
    }
    else {

      const user = await usersService.getUserByUsername(username);

      if (!user) {
        return res.status(404).send({
          status : 404,
          message : "User doesn't exist!"
        });
      }

      const match = await bcrypt.compare(req.body.password, user.password);

      if (match) {
        const payload = {
          _id : user._id,
          username: user.username,
          email: user.email,
          password : user.password,
          counterShows : user.counterShows,
          favouriteShows : user.favouriteShows,
        };

        const accessToken = jwt.sign(payload,
          process.env.MY_SECRET, jwtOpts);

        const obj = {
          token: accessToken,
        };

        return res.status(200).json(obj);
      }
      else {
        console.log("Wrong password!");
        return res.status(404).send({
          status : 404,
          message : "Wrong password!"
        });
      }
    }
  }
  catch (error) {
    next(error);
  }
};

const registerUser = async (req, res, next) => {
  console.log("Inside of server!");
  const username = req.body.username;
  const password = await bcrypt.hash(req.body.password, saltRounds);
  const email = req.body.email;

  try {
    if (!username || !email || !password) {
      const error = new Error('Missing arguments');
      error.status = 400;
      throw error;
    }

    const user = await usersService.getUserByUsername(username);
    if (user) {
      console.log("Username already in use!");
      return res.status(403).send({
        status : 403,
        message : "Username already in use!"
      });
    }

    const newUser = await usersService.registerUser(
      username,
      email,
      password,
    );

    const payload = {
      _id : newUser._id,
      username: newUser.username,
      email: newUser.email,
      password : newUser.password,
      counterShows : newUser.counterShows,
      favouriteShows : newUser.favouriteShows,
    };

    const accessToken = jwt.sign(payload,
      process.env.MY_SECRET || "Random secret", jwtOpts);

    const obj = {
      token: accessToken
    }

    res.status(201).json(obj);
  } catch (error) {
    next(error);
  }
};

const changeUserInfo = async (req, res, next) => {
  const newUsername = req.body.newUsername;
  const oldUsername = req.body.oldUsername;
  const password = await bcrypt.hash(req.body.password, saltRounds);
  const email = req.body.email;

  try {
    if (!email || !newUsername || !oldUsername || !password || !validator.isEmail(email)) {
      return res.status(403).send({
        status : 403,
        message : "Data is not correct!"
      });
    }

    if (newUsername != oldUsername) {
      let user = await usersService.getUserByUsername(newUsername);
      if (user) {
        return res.status(403).send({
          status : 403,
          message : "Username already in use!"
        });
      }
    }
    user = await usersService.updateUserData(oldUsername, newUsername, password, email);

    if (!user) {
      const error = new Error('Cannot update!');
      error.status = 404;
      throw error;
    }

    const payload = {
      _id : user._id,
      username: user.username,
      email: user.email,
      password : user.password,
      counterShows : user.counterShows,
      favouriteShows : user.favouriteShows,
    };


    const accessToken = jwt.sign(payload,
      process.env.MY_SECRET, jwtOpts);

      const obj = {
        token : accessToken,
      };

    return res.status(201).send(obj);
  } catch (err) {
    next(err);
  }
};

const updateUserShows = async (req, res, next) =>  {
  try {
    const name = req.body.titleOfShow;
    const username = req.body.username;
    const shows = await usersService.updateUserShows(username, name);
    if (shows == null) {
      res.status(404).json();
    } else {
      res.status(200).json(shows);
    }
  }
  catch (error) {
    next(error);
  }
};

const getFavouriteShows = async (req, res, next) => {
  const username = req.params.username;
  try {
    if (username == undefined) {
      const error = new Error('Missing username!');
      error.status = 400;
      throw error;
    }
    const shows = await usersService.getFavouriteShows(username);
    if (shows == null) {
      res.status(404).json();
    } else {
      res.status(200).json(shows);
    }
  }
  catch (error) {
    next(error);
  }
};

const updateCounterShows = async (req, res, next) => {
  try {
    const username = req.body.username;
    if (username == undefined) {
      const error = new Error('Missing username!');
      error.status = 400;
      throw error;
    }
    const shows = await usersService.updateCounterShowByUsername(username);
    if (shows == null) {
      res.status(404).json();
    } else {
      res.status(200).json(shows);
    }
  }
  catch (error) {
    next(error);
  }
};

const getCounterShows = async (req, res, next) => {
  try {
    const username = req.params.username;
    if (username == undefined) {
      const error = new Error('Missing username!');
      error.status = 400;
      throw error;
    }
    const shows = await usersService.getCounterShows(username);
    if (shows == null) {
      res.status(404).json();
    } else {
      res.status(200).json(shows);
    }
  }
  catch (error) {
    next(error);
  }
};

const getFavouriteGenre = async (req, res, next) => {
  const username = req.params.username;
  try {
    if (username == undefined) {
      const error = new Error('Missing username!');
      error.status = 400;
      throw error;
    }
    const genre = await usersService.getFavouriteGenre(username);
    if (genre == null) {
      res.status(404).json();
    } else {
      res.status(200).json(genre);
    }
  }
  catch (error) {
    next(error);
  }
};


module.exports = {
  getAllUsers,
  deleteUser,
  getUserById,
  loginUser,
  registerUser,
  getUserByUsername,
  getUsersByUsernamePrefix,
  changeUserInfo,
  getFavouriteShows,
  updateUserShows,
  getFavouriteGenre,
  updateCounterShows,
  getCounterShows
}