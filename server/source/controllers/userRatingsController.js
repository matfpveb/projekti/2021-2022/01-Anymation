const mongoose = require("mongoose");
const db = require("../db/index.js");
const UserRatings = require("../models/userRatingsModel.js");
const userRatingsService = require("../services/userRatingsServices");

const getShowRatings = async (req, res, next) => {
    try {
        const showname = req.query.showname;
        const ratings = await userRatingsService.getShowRatings(showname);
        if (ratings == null){
            res.status(404).json();
        } else {
            res.status(200).send(ratings);
        }
    }
    catch (err) {
        next(err);
    }
};

const updateShowAverageRating = async (req, res, next) => {
    try {
        const showname = req.body.showname;
        const averageRating = await userRatingsService.updateShowAverageRating(showname);
        if (averageRating == null){
            res.status(404).json();
        } else {
            res.status(200).send(averageRating);
        }
    }
    catch (err) {
        next(err);
    }
};

const getShowsRatedByUser = async (req, res, next) => {
    try {
        const username = req.params.username;
        const userRatings = await userRatingsService.getShowsRatedByUser(username);
        if (userRatings == null){
            res.status(404).json();
        } else {
            res.status(200).send(userRatings);
        }
    }
    catch (err) {
        next(err);
    }
};

const getSpecificRating = async (req, res, next) => {
    try {
        const username = req.params.username;
        const showname = req.params.showname;
        const userRating = await userRatingsService.getSpecificRating(username,showname);
        res.status(200).send(userRating);
    }
    catch (err) {
        next(err);
    }
};

const addNewRating = async (req, res, next) => {
    try {
        const title = req.body.title;
        const username = req.body.username;
        const score = req.body.score;
        const userRatings = await userRatingsService.addNewRating(title, username, score);
        if (userRatings == null){
            res.status(404).json();
        } else {
            res.status(200).send(userRatings);
        }
    }
    catch (err) {
        next(err);
    }
};

const updateUserRating = async (req, res, next) =>  {
    try {
      const title = req.body.title;
      const username = req.body.username;
      const newScore = req.body.score;
      const rating = await userRatingsService.updateUserRating(title, username, newScore);
      if (rating == null) {
        res.status(404).json();
      } else {
        res.status(200).json(rating);
      }
    }
    catch (error) {
      next(error);
    }
};

const getTopRatedShow = async (req, res, next) =>  {
    try {
        const username = req.params.username;
        const rating = await userRatingsService.getTopRatedShow(username);
        if (rating == null) {
        res.status(404).json();
        } else {
        res.status(200).json(rating);
        }
    }
    catch (error) {
        next(error);
    }
};

const removeUserRatings = async (req, res, next) =>  {
    try {
        const username = req.body.username;
        const ratings = await userRatingsService.removeUserRatings(username);
        if (ratings == null) {
            res.status(404).json();
        } else {
            res.status(200).json(ratings);
        }
    }
    catch (error) {
        next(error);
    }
};

const updateUserRatingsUsername = async (req, res, next) => {
    try {
        const oldUsername = req.body.oldUsername;
        const newUsername = req.body.newUsername;
        const ratings = await userRatingsService.updateUserRatingsUsername(oldUsername,newUsername);
        if (ratings == null) {
            res.status(404).json();
        } else {
            res.status(200).json(ratings);
        }
    }
    catch (error) {
        next(error);
    }
};

module.exports = {
    getShowRatings,
    updateShowAverageRating,
    getShowsRatedByUser,
    addNewRating,
    updateUserRating,
    getTopRatedShow,
    getSpecificRating,
    removeUserRatings,
    updateUserRatingsUsername
};