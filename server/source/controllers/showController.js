const mongoose = require("mongoose");
const db = require("../db/index.js");
const Show = require("../models/showModel.js");
const showService = require("../services/showServices");


const getAllShows = async (req, res, next) => {
    try {
        const allShows = await showService.getAllShows();
        res.status(200).send(allShows);
    }
    catch (err) {
        next(err);
    }
    
};


const getShowById = async (req, res, next) => {
    const title = req.params.id;
    try {
        if (title == undefined) {
            const error = new Error('Missing id!');
            error.status = 400;
            throw error;
        }
        const show = await showService.getShowById(id);
        if (show == null) {
            res.status(404).json();
        }
        else {
            res.status(200).json(show);
        }
    }
    catch(err) {
        next(err);
    }
};

const getShowByTitle = async (req, res, next) => {
    const title = req.params.title;
    try {
        if (title == undefined) {
            const error = new Error('Missing title!');
            error.status = 400;
            throw error;
        }
        const show = await showService.getShowByTitle(title);
        if (show == null) {
            res.status(404).json();
        }
        else {
            res.status(200).json(show);
        }
    }
    catch(err) {
        next(err);
    }
};

const getAllShowsStartWith = async (req, res, next) => {
    const title = req.params.titlePrefix;
    try {
        if (title == undefined) {
            const error = new Error('Missing title!');
            error.status = 400;
            throw error;
        }
        const shows = await showService.getAllShowsStartWith(title);
        if (shows == null) {
            res.status(404).json();
        }
        else {
            res.status(200).json(shows);
        }
    }
    catch(err) {
        next(err);
    }
};

const getShowByIMDBScore = async (req, res, next) => {
    try {
        const show = await showService.getShowByIMDBScore();
        if (show == null) {
            res.status(404).json();
        }
        else {
            res.status(200).json(show);
        }
    }
    catch(err) {
        next(err);
    }
};
const getShowByScore = async (req, res, next) => {
    try {
        const show = await showService.getShowByScore();
        if (show == null) {
            res.status(404).json();
        }
        else {
            res.status(200).json(show);
        }
    }
    catch(err) {
        next(err);
    }
};


const getAllShowsByGenre = async (req, res, next) => {
    try {
        const genre = req.params.nameOfGenre;
        const page = req.query.page;
        const limit = req.query.limit; 
        const shows = await showService.getAllShowsByGenre(genre,page,limit);
        if (shows == null) {
            res.status(404).json();
        }
        else {
            res.status(200).json(shows);
        }
    }
    catch(err) {
        next(err);
    }
};

const getShowsByGenre = async (req, res, next) => {
    try {
        const genre = req.params.nameOfGenre;
        const shows = await showService.getShowsByGenre(genre);
        if (shows == null) {
            res.status(404).json();
        }
        else {
            res.status(200).json(shows);
        }
    }
    catch(err) {
        next(err);
    }
};

const getShowByScorePaginate = async (req, res, next) => {
    try {
        const page = req.query.page;
        const limit = req.query.limit; 
        const shows = await showService.getShowByScorePaginate("", page,limit);
        if (shows == null) {
            res.status(404).json();
        }
        else {
            res.status(200).json(shows);
        }
    }
    catch(err) {
        next(err);
    }
};



const getShowByIMDBScorePaginate = async (req, res, next) => {
    try {
        const page = req.query.page;
        const limit = req.query.limit; 
        const shows = await showService.getShowByIMDBScorePaginate("", page,limit);
        if (shows == null) {
            res.status(404).json();
        }
        else {
            res.status(200).json(shows);
        }
    }
    catch(err) {
        next(err);
    }
};

const getShowByHotPaginate = async (req, res, next) => {
    try {
        const page = req.query.page;
        const limit = req.query.limit; 
        const shows = await showService.getShowByHotPaginate("", page,limit);
        if (shows == null) {
            res.status(404).json();
        }
        else {
            res.status(200).json(shows);
        }
    }
    catch(err) {
        next(err);
    }
};

const increaseFavouriteNumber = async (req, res, next) => {
    try {
        const name = req.body.titleOfShow;
        const shows = await showService.increaseFavouriteNumber(name);
        if (shows == null) {
            res.status(404).json();
        }
        else {
            res.status(200).json(shows);
        }
    }
    catch(err) {
        next(err);
    }
}

const increaseViewNumber = async (req, res, next) => {
    try {
        const name = req.body.titleOfShow;
        const shows = await showService.increaseViewNumber(name);
        if (shows == null) {
            res.status(404).json();
        }
        else {
            res.status(200).json(shows);
        }
    }
    catch(err) {
        next(err);
    }
}



module.exports = {
    getAllShows,
    getShowByTitle,
    getShowById,
    getAllShowsStartWith,
    getShowByIMDBScore,
    getShowByScore,
    getAllShowsByGenre,
    getShowByScorePaginate,
    getShowByIMDBScorePaginate,
    getShowByHotPaginate,
    increaseFavouriteNumber,
    getShowsByGenre,
    increaseViewNumber
}