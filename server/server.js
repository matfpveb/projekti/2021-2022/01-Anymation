res = require("dotenv").config({path : "./config/dev.env"});


const http = require('http');
const app = require('./source/app.js')

const server = http.createServer(app);
const port = process.env.PORT;

console.log(process.env.PORT);

server.listen(port);
server.once('listening', () => {
    console.log(`Listening on port: ${port}`);
});


