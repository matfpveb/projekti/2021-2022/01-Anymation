![](https://img.shields.io/badge/MongoDB-4EA94B?style=for-the-badge&logo=mongodb&logoColor=white)
![](https://img.shields.io/badge/Angular-DD0031?style=for-the-badge&logo=angular&logoColor=white)
![](https://img.shields.io/badge/Semantic_UI-563D7C?style=for-the-badge&logo=semanticui&logoColor=white)
![](https://img.shields.io/badge/Node.js-43853D?style=for-the-badge&logo=node.js&logoColor=white)

# Project Anymation
# <img src = "client/src/assets/logo.png" width = 500 height = 200>

A website where users can grade animated movies and series, and get fruitful recommendations for their next watch!

___
## :hammer: Build: 

Make sure you have Node installed: [Node](https://nodejs.org/en/download/)

Make sure you have MongoDB installed: [MongoDB](https://www.mongodb.com/try/download/community)

After cloning the repository to a directory of your choosing, navigate to `server` folder using terminal or command line and run:
``` 
npm install 
``` 
Do the same for `client` folder before proceeding to the next step. This will make sure that all dependencies are installed.

### .env
Add in .env file :
```
- PORT = yourPort
- MONGO_URI = yourMongoURI
- MY_SECRET = yourSecret
``` 

___
## :wrench: To run:
Open `server` folder in terminal and run:
``` 
npm start
``` 
then go into `client` folder and run:
``` 
ng serve
``` 
Client is running on 
``` 
http://localhost:4200/
``` 
Visit the link to open the site.

___
## Database schemas
<table>
<tr>
<th>Users</th>
<th>Shows</th>
<th>UserRatings</th>
</tr>
<tr>
<td>

 Field          | Type      | Description                        |
 ---------------| ----------|------------------------------------|
 _id            | String    |                                    |
 username       | String    |                                    |
 password       | String    | Password hash                      |
 email          | String    |                                    |
 counterShows   | Number    | Counts how many shows user watched |
 favouriteShows | [String]  | List of users favourite shows      |

</td>
<td>

 Field           | Type     | Description                              |
 ----------------| ---------|------------------------------------------|
 _id             | String   |                                          |
 title           | String   |                                          |
 score           | String   |    Global score                          |
 genre           | [String] |                                          |
 year            | String   |    Release year                          |
 episodes        | String   |    Number of episoded                    |
 img_url         | String   |    Image url                             |
 description     | String   |                                          |
 our_view_number | Number   | Number of times the show was watched     |
 our_score       | Number   | Average score our users gave to the show |
 favorite_number | Number   | Number of times the show was favourited  |

</td>
<td>

 Field    | Type   | Description              |
 ---------| -------|--------------------------|
 _id      | String |    User ID               |
 showname | String |    Show title            |
 rating   | Number | Users score for the show |

</td>
</tr>
</table>

___
## :video_camera: Demo video 
* [Anymation](https://youtu.be/Y7JJbvX9lm8)
___
## :computer: Developers

- [Vukan Antić, 225/2018](https://gitlab.com/VukanAntic)
- [Tatjana Knežević, 218/2018](https://gitlab.com/TatjanaKnezevic)
- [Divna Mićić, 128/2018](https://gitlab.com/Divna99)
- [Aleksandar Šarbajić, 145/2018](https://gitlab.com/mi18145)
