export interface IUserRatings {
    _id: string;
    showname: string;
    rating: number;
}

export class UserRatings {
    constructor(
        public _id : string,
        public showname: string,
        public rating : number
    ) { }
}


