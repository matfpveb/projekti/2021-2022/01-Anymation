export interface Show {
    _id: string;
    title: string;
    score : string; // imdb score
    genre : [string];
    year : string;
    episodes: string;
    img_url: string;
    description : string;
    our_view_number : number;
    our_score : number;
    favorite_number : number;
}
