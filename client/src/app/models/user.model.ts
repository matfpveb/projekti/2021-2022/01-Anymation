import { Show } from "./show.model";

// ikopirati sta daje get zahtev iz baze

export interface IUser {
    _id: string;
    username: string;
    password: string;
    email: string;
    counterShows: number;
    favouriteShows: [String];
}

export class User {
    constructor(
        public _id : string,
        public username: string,
        public password: string,
        public email: string,
        public counterShows : number,
        public favouriteShows: [String]
    ) { }
}
