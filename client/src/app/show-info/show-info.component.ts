import { Component, Input, OnInit } from '@angular/core';
import { Show } from 'src/app/models/show.model';
import { Observable, Subscription } from 'rxjs';
import { ShowService } from 'src/app/services/shows.service';
import { ActivatedRoute,Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { UserService } from '../services/user.service';
import { User } from '../models/user.model';
import { UserRatingsService } from '../services/user-ratings.service'
declare const $ : any;

@Component({
  selector: 'app-show-info',
  templateUrl: './show-info.component.html',
  styleUrls: ['./show-info.component.css']
})
export class ShowInfoComponent implements OnInit {

  public canFavourite : boolean;
  public canScore : boolean;
  public canAddToShowCounter : boolean;
  public showInfo : Observable<Show[]>;
  private showInfoSub : Subscription;
  private paramMapSub : Subscription;
  private favouriteShowsSub! : Subscription;
  private showsRatedByUserSub! : Subscription;
  private specificRatingSub! : Subscription;
  private newRatingSub! : Subscription;
  private showCounterSub! : Subscription;
  private updateUserRatingsSub! : Subscription;
  private newFavShowSub! : Subscription;
  private incFavNumberSub! : Subscription;
  private incViewNumberSub! : Subscription;
  private updateShowAverageRatingSub! : Subscription;
  @Input()
  public showTitle : string = "";
  public currUser : User | null;

  constructor(private userService : UserService, private showService : ShowService, private userRatingsService : UserRatingsService, private route : ActivatedRoute, private router: Router, private authService: AuthService) {
    this.paramMapSub = this.route.paramMap.subscribe(params => {
      this.showTitle = String(params.get("showName"));
      this.updateUserRatingsSub = this.userRatingsService.updateShowAverageRating(this.showTitle).subscribe(()=>{});
    })
    this.showInfo = this.showService.getShowByTitle(this.showTitle);
    this.showInfoSub = this.showInfo.subscribe(res => {if(!res[0]) router.navigateByUrl("")});
    this.currUser = this.authService.sendUserDataIfExists();
    this.canFavourite = true;
    this.canScore = true;
    this.canAddToShowCounter = true;
   }

  ngOnInit(): void {
    if(this.currUser){
      this.favouriteShowsSub = this.userService.getFavouriteShows(this.currUser!.username).subscribe(res => {
        this.canFavourite = !res.map(show => show.title).includes(this.showTitle);
      })
      this.showsRatedByUserSub = this.userRatingsService.getShowsRatedByUser(this.currUser!.username).subscribe(res => {
        this.canScore = !res.map(rating => rating.showname).includes(this.showTitle);
        this.canAddToShowCounter = this.canScore && this.canFavourite;
      })
      this.specificRatingSub = this.userRatingsService.getSpecificRating(this.currUser!.username,this.showTitle).subscribe(res => {
        if(res != null){
          document.getElementById("score")!.innerText = res.rating.toString();
        }
      })
    }
    this.showInfo = this.showService.getShowByTitle(this.showTitle);
  }
  public changeScore(Score : number) {
    document.getElementById("score")!.innerText = Score.toString();
    if(this.canScore){
      this.newRatingSub = this.userRatingsService.addNewRating(this.showTitle, this.currUser!.username, Score).subscribe(() => {this.updateShowAverageRatingSub = this.userRatingsService.updateShowAverageRating(this.showTitle).subscribe(()=>{});});
      window.alert("Added score to this show!");
      this.canScore = false;
      if (this.canAddToShowCounter){
        this.showCounterSub = this.userService.increaseCounterShows(this.currUser!.username).subscribe(() => { this.canAddToShowCounter = false; });
        this.incViewNumberSub = this.showService.increaseViewNumber(this.showTitle).subscribe(() => {});
      }
    }
    else {
      this.updateUserRatingsSub = this.userRatingsService.updateUserRating(this.showTitle, this.currUser!.username, Score).subscribe(() => {this.updateShowAverageRatingSub = this.userRatingsService.updateShowAverageRating(this.showTitle).subscribe(()=>{});});
      window.alert("Updated score for this show!");
    }
    this.showInfo = this.showService.getShowByTitle(this.showTitle);

  }

  public addToFavourites(){
    if(this.canFavourite){
      this.newFavShowSub = this.userService.addNewFavouriteShow(this.showTitle, this.currUser!.username).subscribe(() => {
      });
      this.incFavNumberSub = this.showService.increaseFavouriteNumber(this.showTitle).subscribe(() => {
        this.canFavourite = false;
      });
      window.alert("This show is added to favourites!");
      if (this.canAddToShowCounter){
        this.showCounterSub = this.userService.increaseCounterShows(this.currUser!.username).subscribe(() => { this.canAddToShowCounter = false; });
        this.incViewNumberSub = this.showService.increaseViewNumber(this.showTitle).subscribe(() => {});
      }
    }
    else{
      window.alert("You already added this show to favourites!");
    }
    this.showInfo = this.showService.getShowByTitle(this.showTitle);
    
  }
  public currentUser():boolean {
    return this.authService.userLoggedIn;
  }
  ngOnDestroy() {
    if (this.paramMapSub != null)
      this.paramMapSub.unsubscribe();
    if (this.showInfoSub != null)
      this.showInfoSub.unsubscribe();
    if (this.favouriteShowsSub != null)
      this.favouriteShowsSub.unsubscribe();
    if (this.showsRatedByUserSub != null)
      this.showsRatedByUserSub.unsubscribe();
    if (this.specificRatingSub != null)
      this.specificRatingSub.unsubscribe();
    if (this.newRatingSub != null)
      this.newRatingSub.unsubscribe();
    if (this.showCounterSub != null)
      this.showCounterSub.unsubscribe();
    if (this.updateUserRatingsSub != null)
      this.updateUserRatingsSub.unsubscribe();
    if (this.newFavShowSub != null)
      this.newFavShowSub.unsubscribe();
    if (this.incFavNumberSub != null)
      this.incFavNumberSub.unsubscribe();
    if(this.incViewNumberSub != null)
      this.incViewNumberSub.unsubscribe();
    if(this.updateShowAverageRatingSub != null)
      this.updateShowAverageRatingSub.unsubscribe();
  }

}


