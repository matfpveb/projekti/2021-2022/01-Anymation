import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { UserService } from '../services/user.service';
import { Show } from '../models/show.model';
import { AuthService } from '../services/auth.service';
import { User } from '../models/user.model';

@Component({
  selector: 'app-my-favourites',
  templateUrl: './my-favourites.component.html',
  styleUrls: ['./my-favourites.component.css']
})
export class MyFavouritesComponent implements OnInit {

  public favouriteShows : Observable<Show[]>;
  public currUser : User | null;
  
  constructor(private userService : UserService, private authService : AuthService) {
    this.currUser = this.authService.sendUserDataIfExists();
    this.favouriteShows = this.userService.getFavouriteShows(this.currUser!.username);

   }


  ngOnInit(): void {
  }

}
