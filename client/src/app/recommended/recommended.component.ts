import { Component, OnInit , OnChanges, Input , OnDestroy} from '@angular/core';
import { Observable , Subscription } from 'rxjs';
import { UserService } from '../services/user.service';
import { Show } from '../models/show.model';
import { AuthService } from '../services/auth.service';
import { User } from '../models/user.model';
import { ShowService } from '../services/shows.service';

@Component({
  selector: 'app-recommended',
  templateUrl: './recommended.component.html',
  styleUrls: ['./recommended.component.css']
})
export class RecommendedComponent implements OnInit, OnDestroy {

  public recommendedShows? : Observable<Show[]> | null;
  public currUser : User | null;
  public recommendedShowsSub! : Subscription | undefined;
  
  constructor(private userService : UserService, private authService : AuthService, private showService : ShowService) {
    this.currUser = this.authService.sendUserDataIfExists();
   }


  ngOnInit(): void {
    this.recommendedShowsSub = this.userService.getFavouriteGenre(this.currUser?.username)?.subscribe(result => {
      this.recommendedShows = this.showService.getShowsByGenre(result);
    });

  }
  ngOnDestroy() : void {
    this.recommendedShowsSub ? this.recommendedShowsSub.unsubscribe() : null;
  }

}
