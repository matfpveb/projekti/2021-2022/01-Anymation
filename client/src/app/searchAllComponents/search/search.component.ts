import { Component, OnDestroy, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { Subscription } from 'rxjs';
declare const $ : any;

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnDestroy {

  public searchPrefix : string = "";
  private paramMapSub : Subscription;

  constructor(private route : ActivatedRoute) {
    this.paramMapSub = this.route.paramMap.subscribe(params => {
      this.searchPrefix = String(params.get("searchPrefix"));
    })
   }

  ngOnInit() : void {
    $(".menu .item").tab();
  }

  ngOnDestroy() {
    if (this.paramMapSub != null) {
      this.paramMapSub.unsubscribe();
    }
  }

}
