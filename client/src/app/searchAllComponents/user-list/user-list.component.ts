import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { IUser } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnChanges {

  public users? : Observable<IUser[]>;

  @Input()
  public searchPrefix! : string;

  constructor(private userService : UserService) { 
  }

  ngOnChanges(): void {
    this.users = this.userService.getUsersPrefix(this.searchPrefix);
  }

}
