import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Show } from 'src/app/models/show.model';
import { ShowService } from 'src/app/services/shows.service';

@Component({
  selector: 'app-show-list',
  templateUrl: './show-list.component.html',
  styleUrls: ['./show-list.component.css']
})
export class ShowListComponent implements OnChanges {

  public shows! : Observable<Show[]>;
  @Input()
  public searchPrefix! : string;

  constructor(private showService : ShowService) {
   }

  ngOnChanges(): void {
    this.shows = this.showService.getShowsPrefix(this.searchPrefix);
  }

}
