import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Show } from 'src/app/models/show.model';
import { ShowService } from 'src/app/services/shows.service';

@Component({
  selector: 'app-most-popular',
  templateUrl: './most-popular.component.html',
  styleUrls: ['./most-popular.component.css']
})
export class MostPopularComponent implements OnInit {

  public showsByScore : Observable<Show[]>;

  constructor(private showService : ShowService) {
    this.showsByScore = this.showService.getShowsByScore();
   }

  ngOnInit(): void {
  }

}
