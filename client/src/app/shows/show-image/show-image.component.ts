import { Component, OnInit, Input } from '@angular/core';
import { Show } from 'src/app/models/show.model';

@Component({
  selector: 'app-show-image',
  templateUrl: './show-image.component.html',
  styleUrls: ['./show-image.component.css']
})
export class ShowImageComponent implements OnInit {

  @Input()
  public show!: Show;

  constructor() { }

  ngOnInit(): void {
  }

}
