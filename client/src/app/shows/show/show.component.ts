import { Component, Input, OnInit } from '@angular/core';
import { Show } from 'src/app/models/show.model';

@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.css']
})
export class ShowComponent implements OnInit {

  @Input()
  public show!: Show;

  constructor() {   }

  ngOnInit(): void {
    
  }

}
