import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-list',
  templateUrl: './home-list.component.html',
  styleUrls: ['./home-list.component.css']
})
export class HomeListComponent implements OnInit {

  public allTopics : string[];
  public isOdd : boolean = false;

  constructor() { 
    this.allTopics = ["Popular", "Best of", "Hot this month"];
    
  }

  ngOnInit(): void {
  }

}
