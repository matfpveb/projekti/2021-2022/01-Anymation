import { Component, Input, OnChanges, OnDestroy, OnInit, HostListener} from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Show } from 'src/app/models/show.model';
import { ShowPagination } from 'src/app/models/showPagination.model';
import { ShowService } from 'src/app/services/shows.service'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnDestroy {

  public numberOfPages : number = 1;
  public currentPage : number = 1;
  public shows! : Observable<Show[] | null>;
  public numOfPagesSub! : Subscription;
  public innerWidth : number;
  public paginationLimit : number;

  @Input()
  public topic!: string;
  @Input()
  public isOdd! : boolean;

  constructor(private showService : ShowService) {
    this.innerWidth = window.innerWidth;
    this.paginationLimit = Math.floor((this.innerWidth - 570)/150.0); 
  }

  ngOnChanges(): void {
    if(this.topic !== undefined) {

        this.numOfPagesSub = this.showService.getNumberOfPagesHome(this.topic).subscribe(pageNumber => {
        this.numberOfPages = pageNumber;
        this.shows = this.showService.getHomePageTopics(this.topic, this.currentPage,this.paginationLimit);
      }) 
    }
  }

  @HostListener('window:resize', ['$event'])
    onResize() {
      this.innerWidth = window.innerWidth;
      this.paginationLimit = Math.floor((this.innerWidth - 480)/160.0);
      this.ngOnChanges();
    }

  ngOnDestroy() : void {
    this.numOfPagesSub ? this.numOfPagesSub.unsubscribe() : null;
  }

  previousPage() {
    this.currentPage -= 1;
    if (this.currentPage == 0)
      this.currentPage = Number(this.numberOfPages);
    this.shows = this.showService.getHomePageTopics(this.topic, this.currentPage,this.paginationLimit);
  }

  nextPage() {
    this.currentPage += 1;
    if (this.currentPage > Number(this.numberOfPages))
      this.currentPage = 1;
    this.shows = this.showService.getHomePageTopics(this.topic, this.currentPage,this.paginationLimit);
  }


}
