import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MostPopularComponent } from './most-popular/most-popular.component';
import { TopRankedComponent } from './top-ranked/top-ranked.component';
import { GenreListComponent } from './genreAllComponents/genre-list/genre-list.component';
import { HomeListComponent } from './homeAllComponents/home-list/home-list.component';
import { MyFavouritesComponent } from './my-favourites/my-favourites.component';
import { RecommendedComponent } from './recommended/recommended.component';
import { UserProfileComponent } from './users/user-profile/user-profile.component';
import { SearchComponent } from './searchAllComponents/search/search.component';
import { ShowInfoComponent } from './show-info/show-info.component';
import { RegisterFormComponent } from './users/register-form/register-form.component';
import { LoginFormComponent } from './users/login-form/login-form.component';
import { UserAuthGuard } from './users/guards/user-auth.guard';

const routes: Routes = [

  {path: '', component: HomeListComponent },
  {path: "most_popular", component: MostPopularComponent},
  {path: "top_ranked", component: TopRankedComponent},
  {path: "genre", component: GenreListComponent},
  {path: "my_favourites", component: MyFavouritesComponent, canActivate : [UserAuthGuard]},
  {path: "recommended", component: RecommendedComponent, canActivate : [UserAuthGuard]},
  {path: "user_profile", component: UserProfileComponent, canActivate : [UserAuthGuard]},
  {path: "search/:searchPrefix", component: SearchComponent},
  {path: "show/:showName", component: ShowInfoComponent},
  {path: "register", component : RegisterFormComponent},
  {path: "login", component : LoginFormComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
