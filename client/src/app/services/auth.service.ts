import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { IUser, User } from '../models/user.model';
import { catchError, map } from 'rxjs/operators';
import { JwtService } from './jwt.service';
import { handleError } from '../serverError/server-error';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private readonly userSubject : Subject<User> = new Subject<User>();
  public readonly user : Observable<User> = this.userSubject.asObservable();
  private isUserLoggedIn : boolean = false;

  private readonly urls = {
    registerUser : "http://localhost:7000/api/user/register",
    loginUser : "http://localhost:7000/api/user/login", 
    changeUserInfo : "http://localhost:7000/api/user/info",
    delete : "http://localhost:7000/api/user/deleteUser",
  };

  constructor(private httpClient : HttpClient, private jwtService : JwtService) { 
    if (jwtService.getToken() != null)
      this.isUserLoggedIn = true;
  }

  public sendUserDataIfExists() : User | null {

    const payload : IUser | null =  this.jwtService.getUserDataFromToken();
    const user = payload ? 
    new User(payload._id, payload.username, payload.password, payload.email, payload.counterShows, payload.favouriteShows) : null;

    if (user != null) {
      this.userSubject.next(user);
      this.isUserLoggedIn = true;
    }
    else {
      this.isUserLoggedIn = false;
    }
    return user;
  }

  public get userLoggedIn() : boolean {
    return this.isUserLoggedIn;
  }

  public set userLoggedIn(isLogged : boolean) {
    this.isUserLoggedIn = isLogged;
  }

  public registerUser(username : string, password : string, email : string) : Observable<User | null> {
    const body = {username, password, email};

    return this.httpClient.post<{token : string}>(this.urls.registerUser, body).pipe(
      catchError((error: HttpErrorResponse) => handleError(error, this.jwtService)),
      map((response : {token : string | null}) => this.mapResponseToUser(response))
    );
  }

  public loginUser(username : string, password : string) : Observable<User | null> {
    const body = {username, password};
    return this.httpClient.post<{token : string}>(this.urls.loginUser, body).pipe(
      catchError((error : HttpErrorResponse) => handleError(error, this.jwtService)),
      map((response : {token : string | null}) => this.mapResponseToUser(response))
    );
  }

  public patchDataUserData(oldUsername : string, newUsername : string, password : string, email : string) {
    const body = {oldUsername, newUsername, password, email};

    const headers: HttpHeaders  =  new HttpHeaders().append("x-auth-token", String(this.jwtService.getToken()));
    return this.httpClient.patch<{token : string}>(this.urls.changeUserInfo, body, {headers}).pipe(
      catchError((error : HttpErrorResponse) => handleError(error, this.jwtService)),
      map((response : {token : string | null}) => this.mapResponseToUser(response))
    );
  }

  public deleteUser(username : string) {
    const body = {username};
    const headers: HttpHeaders  =  new HttpHeaders().append("x-auth-token", String(this.jwtService.getToken()));
    const options = {headers, body};

    console.log(options);

    this.logoutUser();

    return this.httpClient.delete<any>(this.urls.delete, options).pipe(
      catchError((error : HttpErrorResponse) => handleError(error, this.jwtService)),
    );
  }

  public logoutUser() : void {
    this.jwtService.removeToken();
    this.isUserLoggedIn = false;
    this.userSubject.next(undefined);
  }

  private mapResponseToUser(response: { token: string | null }): User | null{
    if (response.token != null)
      this.jwtService.setToken(response.token);
    return this.sendUserDataIfExists();
  }

}
