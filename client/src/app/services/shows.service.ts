import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {  map } from 'rxjs/operators';
import { Show } from 'src/app/models/show.model';
import { ShowPagination } from 'src/app/models/showPagination.model';
import { JwtService } from './jwt.service';

@Injectable({
  providedIn: 'root'
})
export class ShowService {
  public numberOfPages : number = 0;
  
  private urls = {

    showsPrefix : "http://localhost:7000/api/shows/titlePrefix/",
    getShowsByIMDBScore : "http://localhost:7000/api/shows/IMDBScore/",
    getShowsByScore: "http://localhost:7000/api/shows/Score/",
    allShows:"http://localhost:7000/api/shows/",
    showsGenre:"http://localhost:7000/api/shows/Genre/",
    showsFavGenre:"http://localhost:7000/api/shows/FavouriteGenre/",
    getShowsByHot: "http://localhost:7000/api/shows/Hot/",
    increaseFavouriteNumber : "http://localhost:7000/api/shows/increaseFavourite/",
    increaseViewNumber : "http://localhost:7000/api/shows/increaseView/"
  };

  constructor(private httpService : HttpClient,  private jwtService : JwtService) { }

  public getShowsPrefix(prefixOfShow : string = "") : Observable<Show[]> {

    var urlCall : string = "";

    if (prefixOfShow.trim() == "") {
      urlCall = this.urls.allShows;
    }
    else {
      urlCall = this.urls.showsPrefix + prefixOfShow;
    }

    const observable : Observable<Show[]> = this.httpService.get<Show[]>( urlCall );

    return observable;
    
  }

  public increaseFavouriteNumber(titleOfShow : string): Observable<Show> {
    const body = {titleOfShow};
    return this.httpService.patch<Show>(this.urls.increaseFavouriteNumber, body);
  }

  public increaseViewNumber(titleOfShow : string): Observable<Show> {
    const body = {titleOfShow};
    return this.httpService.patch<Show>(this.urls.increaseViewNumber, body);
  }

  public getShowsByIMDB() : Observable<Show[]> {
    const observable : Observable<Show[]> = this.httpService.get<Show[]>(this.urls.getShowsByIMDBScore);
    return observable;   
  }
  public getShowsByScore() : Observable<Show[]> {
    const observable : Observable<Show[]> = this.httpService.get<Show[]>(this.urls.getShowsByScore);
    return observable;   
  }
  
  public getAllShows() : Observable<Show[]> {
    const observable : Observable<Show[]> = this.httpService.get<Show[]>(this.urls.allShows);
    return observable;
    
  }
  public getShowByTitle(titleOfShow : string) : Observable<Show[]> {
    const observable : Observable<Show[]> = this.httpService.get<Show[]>(this.urls.allShows+titleOfShow);
    return observable;
    
  }

  public getNumberOfPages(genre : string, page : number = 1, limit : number = 9) : Observable<number> {
    const queryParams : HttpParams = new HttpParams().append("page", page.toString()).append("limit", limit.toString());
    const observable : Observable<number> = this.httpService.get<ShowPagination>(this.urls.showsGenre + genre + "/",{params : queryParams})
    .pipe(
      map((el) => {
        return el.totalPages;
      })
    );
    return observable;
  }

  public getShowsByGenrePagination(nameOfGenre : string, page : number = 1, limit : number = 9) : Observable<Show[]|null>{
    const queryParams : HttpParams = new HttpParams().append("page", page.toString()).append("limit", limit.toString());
    const observable : Observable<ShowPagination> = this.httpService.get<ShowPagination>(this.urls.showsGenre + nameOfGenre + "/",{params : queryParams});
    return observable.pipe(
      map((pagination: ShowPagination) => {
        if (page <= pagination.totalPages)
          return pagination.docs;
        else
          return null;
      })
    )
  }

  public getShowsByGenre(nameOfGenre : String | undefined) : Observable<Show[]>| null {
    if (nameOfGenre != undefined){
      const observable : Observable<Show[]> = this.httpService.get<Show[]>(this.urls.showsFavGenre +nameOfGenre);
      return observable;
    }
    return null;
  }


  public getNumberOfPagesHome(path : string, page : number = 1, limit : number = 9) : Observable<number> {
    const queryParams : HttpParams = new HttpParams().append("page", page.toString()).append("limit", limit.toString());
    let url = "";
    if (path == "Popular")
      url = this.urls.getShowsByScore;
    else if (path == "Best of")
      url = this.urls.getShowsByIMDBScore;
    else
      url = this.urls.getShowsByHot;
    const observable : Observable<number> = this.httpService.get<ShowPagination>(url + path + "/",{params : queryParams})
    .pipe(
      map((el) => {
        return el.totalPages;
      })
    );
    return observable;
  }

  public getHomePageTopics(path : string, page : number = 1, limit : number = 9): Observable<Show[]|null>{
    if (path == "Popular")
    {
      return this.getShowsByScorePagination(path, page, limit);
    }
    else if (path == "Best of")
    {
      return this.getShowsByIMDBScorePagination(path, page, limit);
    }
    else {
      return this.getShowsByHotPagination(path, page, limit);
    }
  }

  public getShowsByScorePagination(path : string, page : number = 1, limit : number = 9) : Observable<Show[]|null>{
    const queryParams : HttpParams = new HttpParams().append("page", page.toString()).append("limit", limit.toString());
    const observable : Observable<ShowPagination> = this.httpService.get<ShowPagination>(this.urls.getShowsByScore + path + "/",{params : queryParams});
    return observable.pipe(
      map((pagination: ShowPagination) => {
        if (page <= pagination.totalPages)
          return pagination.docs;
        else
          return null;
      })
    )
  }

  public getShowsByIMDBScorePagination(path : string, page : number = 1, limit : number = 9) : Observable<Show[]|null>{
    const queryParams : HttpParams = new HttpParams().append("page", page.toString()).append("limit", limit.toString());
    const observable : Observable<ShowPagination> = this.httpService.get<ShowPagination>(this.urls.getShowsByIMDBScore + path + "/",{params : queryParams});
    return observable.pipe(
      map((pagination: ShowPagination) => {
        if (page <= pagination.totalPages)
          return pagination.docs;
        else
          return null;
      })
    )
  }

  public getShowsByHotPagination(path : string, page : number = 1, limit : number = 9) : Observable<Show[]|null>{
    const queryParams : HttpParams = new HttpParams().append("page", page.toString()).append("limit", limit.toString());
    const observable : Observable<ShowPagination> = this.httpService.get<ShowPagination>(this.urls.getShowsByHot + path + "/",{params : queryParams});
    return observable.pipe(
      map((pagination: ShowPagination) => {
        if (page <= pagination.totalPages)
          return pagination.docs;
        else
          return null;
      })
    )
  }
}
