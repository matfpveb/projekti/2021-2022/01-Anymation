import { Injectable } from '@angular/core';
import { IUser } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class JwtService {

  public USER_ID : string = "USER_JWT_TOKEN";

  constructor() { }

  public getToken() : string | null {
    const token : string | null = localStorage.getItem(this.USER_ID);
    if (!token) {
      return null;
    }
    return token;
  }

  public getUserDataFromToken() : IUser | null {
    const token = this.getToken();

    if (!token)
      return null;


    const payloadString = token.split(".")[1];
    const jsonData = window.atob(payloadString);
    var payload : IUser = JSON.parse(jsonData);
    return payload;
  }

  public setToken(jwt : string) : void {
    localStorage.setItem(this.USER_ID, jwt);
  }

  public removeToken() : void {
    localStorage.removeItem(this.USER_ID);
  } 
}
