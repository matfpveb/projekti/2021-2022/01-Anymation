import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IUser, User } from '../models/user.model';
import { JwtService } from './jwt.service';
import { AuthService } from './auth.service';
import { tap, map } from 'rxjs/operators';
import { Show } from 'src/app/models/show.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {


  private urls = {

    usersPrefix : "http://localhost:7000/api/user/usernamePrefix/",
    allUsers : "http://localhost:7000/api/user/",
    changeUserInfo : "http://localhost:7000/api/user/info",
    UserById : "http://localhost:7000/api/id/",
    getFavouriteShows : "http://localhost:7000/api/user/favouriteShows/",
    getFavouriteGenre : "http://localhost:7000/api/user/favouriteGenre/",
    updateCounterShows : "http://localhost:7000/api/user/counterShowsUpdate",
    getCounterShows : "http://localhost:7000/api/user/getCounterShows/",
    updateFavouriteShows : "http://localhost:7000/api/user/favouriteShowsUpdate"
  };

  constructor(private httpService : HttpClient, private jwt: JwtService, private auth: AuthService
) { 
    
  }

  public getUsersPrefix(prefixOfUsername : string = "") : Observable<IUser[]> {

    var urlCalled : string = "";
    if (prefixOfUsername.trim() == "") {
      urlCalled = this.urls.allUsers;
    }
    else {
      urlCalled = this.urls.usersPrefix + prefixOfUsername;
    }
    const observable : Observable<IUser[]> = this.httpService.get<IUser[]>(urlCalled);
    return observable;
    
  }


  public emails: Array<string> = [];
  private allUsers: IUser[] = [];
  public currentUser!: User;

  public getUsers(): IUser[] {
    return this.allUsers;
  }

  public getEmails(): Array<string> {
    this.allUsers.forEach(element => {
      this.emails.push(element.email.toString());
    });
    return this.emails;
  }

  public putCurrentUser(user: User) {
    this.currentUser = user;
  }

  public getCurrentUser(): User | null{
    return this.currentUser;
  }

  public removeCurrentUser(): void {
    this.currentUser = {} as IUser;
  }

  public getFavouriteShows(username : string) : Observable<Show[]> {
    const observable : Observable<Show[]> = this.httpService.get<Show[]>(this.urls.getFavouriteShows + username);
    return observable; 
  }

  public getFavouriteGenre(username : string | undefined) : Observable<String> | null {
    if (username != undefined){
      const observable : Observable<String> = this.httpService.get<String>(this.urls.getFavouriteGenre + username);
      return observable; 
    }
    return null;
  }
  public increaseCounterShows(username : string): Observable<User> {
    const body = {username};
    return this.httpService.patch<User>(this.urls.updateCounterShows, body);
  }

  public getCounterShows(username : string | undefined) : Observable<Number> | null {
    if (username != undefined){
      const observable : Observable<Number> = this.httpService.get<Number>(this.urls.getCounterShows + username);
      return observable; 
    }
    return null;
  }

  public addNewFavouriteShow (titleOfShow : string, username : string) : Observable<User>  {
    const body = {titleOfShow, username};
    return this.httpService.patch<User>(this.urls.updateFavouriteShows, body);    
  }

}


