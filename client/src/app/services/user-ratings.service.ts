import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../models/user.model';
import { UserRatings } from '../models/userRatings';

@Injectable({
  providedIn: 'root'
})
export class UserRatingsService {

  private urls = {

    addNewRating : "http://localhost:7000/api/userRatings/addNewRating",
    updateUserRating : "http://localhost:7000/api/userRatings/updateUserRating",
    getTopRatedShow : "http://localhost:7000/api/userRatings/getTopRatedShow/",
    getShowsRatedByUser: "http://localhost:7000/api/userRatings/getShowsRatedByUser/",
    getSpecificRating: "http://localhost:7000/api/userRatings/getSpecificRating/",
    updateShowAverageRating: "http://localhost:7000/api/userRatings/updateShowAverageRating",
    removeUserRatings: "http://localhost:7000/api/userRatings/removeUserRatings",
    updateUserRatingsUsername: "http://localhost:7000/api/userRatings/updateUserRatingsUsername"

  };

  constructor(private httpClient : HttpClient) { }

  public addNewRating(title : string, username : string, score : number) : Observable<UserRatings> {
    const body = {title, username, score};
    return this.httpClient.post<UserRatings>(this.urls.addNewRating, body);
  }

  public updateUserRating(title : string, username : string, score : number) : Observable<UserRatings>  {
    const body = {title, username, score};
    return this.httpClient.patch<UserRatings>(this.urls.updateUserRating, body);
  }

  public getTopRatedShow(username : string | undefined) : Observable<String> | null  {
    if(username)
      return this.httpClient.get<String>(this.urls.getTopRatedShow+username);
    return null;
  }

  public getShowsRatedByUser(username : string) : Observable<UserRatings[]> {
    return this.httpClient.get<UserRatings[]>(this.urls.getShowsRatedByUser+username);
  }

  public getSpecificRating(username: string, showname: string) : Observable<UserRatings | null> {
    const rating = this.httpClient.get<UserRatings | null>(this.urls.getSpecificRating+username+"/"+showname);
    return rating;
  }

  public updateShowAverageRating(showname: string) : Observable<UserRatings> {
    const body = {showname};
    return this.httpClient.patch<UserRatings>(this.urls.updateShowAverageRating, body);
  }
  
  public removeUserRatings(username: string) : Observable<UserRatings> {
    const body = {username};
    return this.httpClient.patch<UserRatings>(this.urls.removeUserRatings, body);
  }

  public updateUserRatingsUsername(oldUsername: string, newUsername: string) : Observable<UserRatings[]> {
    const body = {oldUsername,newUsername}
    return this.httpClient.patch<UserRatings[]>(this.urls.updateUserRatingsUsername, body);
  }
}
