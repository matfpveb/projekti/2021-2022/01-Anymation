import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Show } from 'src/app/models/show.model';
import { ShowService } from 'src/app/services/shows.service';

@Component({
  selector: 'app-top-ranked',
  templateUrl: './top-ranked.component.html',
  styleUrls: ['./top-ranked.component.css']
})
export class TopRankedComponent implements OnInit {

  public showsByIMDB : Observable<Show[]>;

  constructor(private showService : ShowService) {
    this.showsByIMDB = this.showService.getShowsByIMDB();
   }

  ngOnInit(): void {
  }

}
