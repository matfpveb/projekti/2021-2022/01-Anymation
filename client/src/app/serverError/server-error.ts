import { HttpErrorResponse, HttpResponse } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { JwtService } from "../services/jwt.service";

export function handleError (error : HttpErrorResponse, jwtService : JwtService) : Observable<{token : string | null}>{

    //console.log(error);
    const serverError : {message : string, status : number, stack : string} = error.error;
    window.alert(`Server error : ${serverError.message} (${serverError.status})`);
    return of({token : jwtService.getToken()});
    

}
