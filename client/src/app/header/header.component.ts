import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public searchString : string;

  ngOnInit(): void {
  }

  public currentUser():boolean {
    return this.authService.userLoggedIn;
  }

  public onKeydown(ev : Event) {
    if (ev.target as HTMLInputElement != null) {
      this.searchString = (ev.target as HTMLInputElement).value;
    }
    if (this.searchString.trim() === "") {
      window.alert("Must enter a search request!");
    }
    else 
      this.router.navigate(['/search', this.searchString]);
  }

  constructor (private authService: AuthService, private router: Router, private route: ActivatedRoute) 
  {
    this.searchString = "";
  }

}
