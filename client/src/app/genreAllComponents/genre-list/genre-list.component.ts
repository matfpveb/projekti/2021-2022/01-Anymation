import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-genre-list',
  templateUrl: './genre-list.component.html',
  styleUrls: ['./genre-list.component.css']
})
export class GenreListComponent implements OnInit {

  public allGenres : string[];
  public isOdd : boolean = false;

  constructor() { 
    this.allGenres = ["Adventure", "Comedy", "Family", "Fantasy", "Musical", "Romance" , "Shounen"];
    
  }

  ngOnInit(): void {
  }

}
