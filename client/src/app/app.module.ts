import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { MostPopularComponent } from './most-popular/most-popular.component';
import { TopRankedComponent } from './top-ranked/top-ranked.component';
import { MyFavouritesComponent } from './my-favourites/my-favourites.component';
import { RecommendedComponent } from './recommended/recommended.component';
import { ShowComponent } from './shows/show/show.component';
import { ShowListComponent } from './searchAllComponents/show-list/show-list.component';
import { SearchComponent } from './searchAllComponents/search/search.component';
import { ShowImageComponent } from './shows/show-image/show-image.component';
import { ShowInfoComponent } from './show-info/show-info.component';
import { UserProfileComponent } from './users/user-profile/user-profile.component';
import { UserComponent } from './users/user/user.component';
import { UserListComponent } from './searchAllComponents/user-list/user-list.component';
import { RegisterFormComponent } from './users/register-form/register-form.component';
import { LoginFormComponent } from './users/login-form/login-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { GenreListComponent } from './genreAllComponents/genre-list/genre-list.component';
import { GenreComponent } from './genreAllComponents/genre/genre.component';
import { HomeListComponent } from './homeAllComponents/home-list/home-list.component';
import { HomeComponent } from './homeAllComponents/home/home.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MostPopularComponent,
    TopRankedComponent,
    MyFavouritesComponent,
    RecommendedComponent,
    ShowComponent,
    ShowListComponent,
    SearchComponent,
    ShowImageComponent,
    ShowInfoComponent,
    UserProfileComponent,
    UserComponent,
    UserListComponent,
    RegisterFormComponent,
    LoginFormComponent,
    GenreListComponent,
    GenreComponent,
    HomeListComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
