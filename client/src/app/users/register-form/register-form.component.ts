import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent implements OnDestroy {

  registerForm: FormGroup;
  registerSub?: Subscription;

  // private authService : AuthService, 
  // we will need this service, but for now, let it be commented
  constructor(private router: Router, private authService: AuthService) {
    this.registerForm = new FormGroup({
      username: new FormControl("", [Validators.required, Validators.pattern(/^[0-9a-zA-Z_-]{8,}$/)]), // validatiors go after this!
      password: new FormControl("", [Validators.required, Validators.pattern(/^[0-9a-zA-Z_!@#$%^&*()+=`~-]{4,}$/)]),
      email: new FormControl("", [Validators.required, Validators.email])
    });
  }

  public ngOnDestroy(): void {
    this.registerSub ? this.registerSub.unsubscribe() : null;
  }

  public register(): void {
    if (this.registerForm.invalid) {
      window.alert("The form is not valid!");
      return;
    }

    const formData = this.registerForm.value;
    this.registerSub = this.authService
      .registerUser(formData.username, formData.password, formData.email)
      .subscribe(() => {
        this.router.navigateByUrl("/");
      });
  }



}
