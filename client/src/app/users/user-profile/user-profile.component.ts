import { Component, Input, OnChanges, OnInit, OnDestroy} from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { User } from '../../models/user.model';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';
import { UserRatingsService } from '../../services/user-ratings.service'
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit, OnDestroy {

  @Input() user: User | null = null;
  shouldDisplayUserForm: boolean = false;
  shouldDisplayUserInfo: boolean = true;
  userForm: FormGroup;
  favoriteGenre : String | undefined;
  counterShows : Number | undefined;
  favouriteShow : String | undefined;
  private patchDataUserDataSub! : Subscription;
  private deleteUserSub! : Subscription;
  private favouriteGenreSub! : Subscription | undefined;
  private counterShowsSub! : Subscription | undefined;
  private topRatedShowSub! : Subscription | undefined;
  private updateUserRatingsUsernameSub! : Subscription
  private removeUserRatingsSub! : Subscription;


  constructor(private userService: UserService, private authService: AuthService, private formBuilder: FormBuilder, private router: Router, private userRatingsService : UserRatingsService) {
    this.user = authService.sendUserDataIfExists();
    this.userForm = this.initializeUserFormGroup();
  }

  initializeUserFormGroup(): FormGroup {
    return new FormGroup({
      username: new FormControl(this.user?.username, [Validators.required, Validators.pattern(/[a-zA-Z0-9_-]{8,}/)]),
      email: new FormControl(this.user?.email, [Validators.required, Validators.email]),
      password: new FormControl("", [Validators.required, Validators.pattern(/^[0-9a-zA-Z_!@#$%^&*()+=`~-]{4,}$/)]),
    });
  }

  onChangeInfo() {
    this.shouldDisplayUserForm = true;
    this.shouldDisplayUserInfo = false;
  }

  onSaveChanges() {
    this.shouldDisplayUserForm = false;
    this.shouldDisplayUserInfo = true;
  }


  onUserFormSubmit() {
    if (this.userForm.invalid) {
      window.alert('Form is not valid!');
      return;
    }

    const formData = this.userForm.value;
    this.user = this.authService.sendUserDataIfExists();
    const oldUsername: string | null = (this.user?.username == undefined) ? null : this.user?.username;

    this.patchDataUserDataSub = this.authService.patchDataUserData(String(oldUsername), formData.username, formData.password, formData.email)
      .subscribe(() => {
        this.updateUserRatingsUsernameSub = this.userRatingsService.updateUserRatingsUsername(String(oldUsername), formData.username).subscribe(() => {});
        this.user = this.authService.sendUserDataIfExists();
        this.onSaveChanges();
      });
  }

  deleteUser() {
    if (confirm("Are you sure you want to delete your account?")) {
      if (this.user?.username != undefined){
        this.deleteUserSub = this.authService.deleteUser(this.user?.username).subscribe(() => {
          window.alert("User successfully deleted!");
          this.router.navigateByUrl("");
        });
        this.removeUserRatingsSub = this.userRatingsService.removeUserRatings(this.user!.username).subscribe(() => {});
      }
      else {
        window.alert("Something went wrong, try again!");
        this.router.navigateByUrl("");
      }
    }
  }

  logout(): void {
    this.authService.logoutUser();
    this.router.navigate(['']);
  }


  ngOnInit(): void {
    this.user = this.authService.sendUserDataIfExists();
    this.favouriteGenreSub = this.userService.getFavouriteGenre(this.user?.username)?.subscribe(res => this.favoriteGenre = res);
    this.counterShowsSub = this.userService.getCounterShows(this.user?.username)?.subscribe(res => this.counterShows = res);
    this.topRatedShowSub = this.userRatingsService.getTopRatedShow(this.user?.username)?.subscribe(res => this.favouriteShow = res);
  }

  ngOnDestroy() : void {
    this.patchDataUserDataSub ? this.patchDataUserDataSub.unsubscribe() : null;
    this.deleteUserSub ? this.deleteUserSub.unsubscribe() : null;
    this.favouriteGenreSub ? this.favouriteGenreSub.unsubscribe() : null;
    this.counterShowsSub ? this.counterShowsSub.unsubscribe() : null;
    this.topRatedShowSub ? this.topRatedShowSub.unsubscribe() : null;
    this.updateUserRatingsUsernameSub ? this.updateUserRatingsUsernameSub.unsubscribe : null;
    this.removeUserRatingsSub ? this.removeUserRatingsSub.unsubscribe : null;
  }

}
